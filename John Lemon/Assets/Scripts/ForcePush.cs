﻿using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Security.Cryptography;
using UnityEngine;


//ForcePush script based off of vidoes by user Partum Game Tutorials on Youtube
public class ForcePush : MonoBehaviour
{

    public float pushAmount;
    public float pushRadius;

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.G))
        {
            DoPush();
        }
    }

    private void DoPush()
    {
        Collider[] colliders = Physics.OverlapSphere(transform.position, pushRadius);

        foreach (Collider pushedObjec in colliders)
        {
            if (pushedObjec.CompareTag("Enemy"))
            {
                Rigidbody pushedBody = pushedObjec.GetComponent<Rigidbody>();

                pushedBody.AddExplosionForce(pushAmount, Vector3.up, pushRadius);
            }
        }
    }
}
