﻿using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using UnityEngine;

public class Pushing : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        //Based off of tutorial from Youtube by user Arshel Tutorials

            if (Input.GetKey(KeyCode.Mouse0))
            {
                this.GetComponent<Rigidbody>().AddForce(Vector3.left * 100.0f);
            }

            if (Input.GetKey(KeyCode.Mouse1))
            {
                this.GetComponent<Rigidbody>().AddForce(Vector3.right * 100.0f);
            }
    }
}
